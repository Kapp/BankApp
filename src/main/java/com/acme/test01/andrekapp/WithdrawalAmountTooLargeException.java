package com.acme.test01.andrekapp;

/**
 * Exception class to be used when the withdrawal amount is above the limit.
 */
public class WithdrawalAmountTooLargeException extends Exception {

    /**
     * Constructs an instance of <code>WithdrawalAmountTooLargeException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public WithdrawalAmountTooLargeException(String msg) {
        super(msg);
    }

    /**
     * Constructs an instance of <code>WithdrawalAmountTooLargeException</code> with the
     * specified cause.
     *
     * @param cause the cause.
     */
    public WithdrawalAmountTooLargeException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs an instance of <code>WithdrawalAmountTooLargeException</code> with the
     * specified detail message and cause.
     *
     * @param msg   the detail message.
     * @param cause the cause.
     */
    public WithdrawalAmountTooLargeException(String message, Throwable cause) {
        super(message, cause);
    }
}

