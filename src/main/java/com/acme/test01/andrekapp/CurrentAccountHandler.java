package com.acme.test01.andrekapp;

/**
 * This class will be used to handle the business rules for Current accounts.
 */
public class CurrentAccountHandler {

    public void checkWithdrawRules(CurrentAccount currentAccount, int withdrawalAmount) throws WithdrawalAmountTooLargeException {

        checkAccountMinimumBalance(currentAccount, withdrawalAmount);
    }

    /**
     * CurrentAccounts balance are not allowed to drop below the overdraft limit as per constants setting.
     *
     * @param currentAccount
     * @param withdrawalAmount
     * @throws WithdrawalAmountTooLargeException
     */
    private void checkAccountMinimumBalance(CurrentAccount currentAccount, int withdrawalAmount) throws WithdrawalAmountTooLargeException {

        System.out.println("Account bal  : " + currentAccount.getAccountBalance());
        System.out.println("Account over : " + currentAccount.getOverdraft());
        System.out.println("With amt     : " + withdrawalAmount);

        //  1000  + overdraft - with < current limit
        if ((currentAccount.getAccountBalance().intValue() + currentAccount.getOverdraft() - withdrawalAmount) < TransactionConstants.CURRENT_MAX_OVERDRAFT_LIMIT *-1) {
            throw new WithdrawalAmountTooLargeException("Withdrawal will take account below minimum balance");
        }
    }
}
