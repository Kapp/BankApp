package com.acme.test01.andrekapp;

/**
 * Exception class for throwing an exception when the account is not found.
 */
public class AccountNotFoundException extends RuntimeException {

    /**
     * Constructs an instance of <code>AccountNotFoundException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public AccountNotFoundException(String msg) {
        super(msg);
    }

    /**
     * Constructs an instance of <code>AccountNotFoundException</code> with the
     * specified cause.
     *
     * @param cause the cause.
     */
    public AccountNotFoundException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs an instance of <code>AccountNotFoundException</code> with the
     * specified detail message and cause.
     *
     * @param msg   the detail message.
     * @param cause the cause.
     */
    public AccountNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}

