package com.acme.test01.andrekapp;

/**
 * This class will be used to handle the business rules for Current accounts.
 */
public class SavingsAccountHandler {

    public void checkWithdrawRules(SavingsAccount savingsAccount, int withdrawalAmount) throws WithdrawalAmountTooLargeException {

        checkAccountMinimumBalance(savingsAccount, withdrawalAmount);
    }

    /**
     * Savings account are not allowed to fall below the savings account minimum balance as per constants.
     *
     * @param savingsAccount
     * @param withdrawalAmount
     * @throws WithdrawalAmountTooLargeException
     */
    private void checkAccountMinimumBalance(SavingsAccount savingsAccount, int withdrawalAmount) throws WithdrawalAmountTooLargeException {
        if ((savingsAccount.getAccountBalance().intValue() - withdrawalAmount) < TransactionConstants.SAVING_MINIMUM_BALANCE) {
            throw new WithdrawalAmountTooLargeException("Withdrawal will take account below minimum balance");
        }
    }


}
