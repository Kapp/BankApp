package com.acme.test01.andrekapp;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Singleton class implementing the database functionality to simulate a real database. Changes will NOT be persisted when the application restarts.
 */
public class SystemDB {

    private static SystemDB instance = new SystemDB();

    private static Map<Long, SavingsAccount> savingAccountsMap;
    private static Map<Long, CurrentAccount> currentAccountsMap;

    /**
     * Private constructor to prevent instantiation. Do instance constructor in private constructor. No need for double locking.
     */
    private SystemDB() {
        /*
            Use ConcurrentHashMap as implementation for thread safety.
         */
        savingAccountsMap = new ConcurrentHashMap<Long, SavingsAccount>();
        currentAccountsMap = new ConcurrentHashMap<Long, CurrentAccount>();

                /*
            Instantiate two test savings accounts
         */
        SavingsAccount savingsAccount1 = new SavingsAccount();
        savingsAccount1.setAccountBalance(2000);
        savingsAccount1.setCustomerNumber("1");
        savingsAccount1.setId(Long.valueOf(1));

        SavingsAccount savingsAccount2 = new SavingsAccount();
        savingsAccount2.setAccountBalance(5000);
        savingsAccount2.setCustomerNumber("2");
        savingsAccount2.setId(Long.valueOf(2));


        savingAccountsMap.put(savingsAccount1.getId(), savingsAccount1);
        savingAccountsMap.put(savingsAccount2.getId(), savingsAccount2);

        CurrentAccount currrentAccount3 = new CurrentAccount();
        currrentAccount3.setAccountBalance(1000);
        currrentAccount3.setCustomerNumber("3");
        currrentAccount3.setOverdraft(10000);
        currrentAccount3.setId(Long.valueOf(3));


        CurrentAccount currrentAccount4 = new CurrentAccount();
        currrentAccount4.setAccountBalance(-5000);
        currrentAccount4.setCustomerNumber("4");
        currrentAccount4.setOverdraft(20000);
        currrentAccount4.setId(Long.valueOf(4));

        currentAccountsMap.put(currrentAccount3.getId(), currrentAccount3);
        currentAccountsMap.put(currrentAccount4.getId(), currrentAccount4);
    }


    /**
     * Retrieve the account using the accountId as key.
     *
     * @param accountId
     * @return
     * @throws AccountNotFoundException
     */
    public static Account getAccount(Long accountId) throws AccountNotFoundException {

        if (!savingAccountsMap.containsKey(accountId) && !currentAccountsMap.containsKey(accountId)) {
            throw new AccountNotFoundException("Account does not exists");
        }

        if (savingAccountsMap.containsKey(accountId)) {
            return savingAccountsMap.get(accountId);
        } else {
            return currentAccountsMap.get(accountId);
        }

    }

}
