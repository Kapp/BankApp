package com.acme.test01.andrekapp;

/**
 * Interface defining the actions on the different account types that can be implemented.
 */
public interface AccountService {

    void openSavingsAccount(Long accountId, Long amountToDeposit);

    void openCurrentAccount(Long accountId);

    void withdraw(Long accountId, int amountToWithdraw)
            throws AccountNotFoundException, WithdrawalAmountTooLargeException;

    void deposit(Long accountId, int amountToDeposit)
            throws AccountNotFoundException;
}
