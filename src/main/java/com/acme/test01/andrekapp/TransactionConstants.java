package com.acme.test01.andrekapp;

/**
 * Class used to set the transaction limits for the different account types. This should ideally be populated by a properties file or read from a database.
 */
public class TransactionConstants {

    /**
     * Private constructor to prevent instantiation.
     */
    private TransactionConstants() {
    }

    public static final int SAVING_MINIMUM_BALANCE = 1000;
    public static final int SAVING_MINIMUM_DEPOSIT = 1000;

    public static final int CURRENT_MAX_OVERDRAFT_LIMIT = 100000;
}
