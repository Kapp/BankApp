package com.acme.test01.andrekapp;

public class AccountServiceImpl implements AccountService {

    private SavingsAccountHandler savingsAccountHandler = new SavingsAccountHandler();
    private CurrentAccountHandler currentAccountHandler = new CurrentAccountHandler();

    /**
     * TODO - To be implemented
     *
     * @param accountId
     * @param amountToDeposit
     */
    public void openSavingsAccount(Long accountId, Long amountToDeposit) {

    }

    /**
     * TODO - To be implemented
     *
     * @param accountId
     */
    public void openCurrentAccount(Long accountId) {

    }

    /**
     * @param accountId
     * @param amountToWithdraw
     * @throws AccountNotFoundException
     * @throws WithdrawalAmountTooLargeException
     */
    public void withdraw(Long accountId, int amountToWithdraw)
            throws AccountNotFoundException, WithdrawalAmountTooLargeException {

        /*
            Retrieve the account
         */
        Account account = SystemDB.getAccount(accountId);

        /*
            Check account limits
         */
        if (account instanceof SavingsAccount) {
            savingsAccountHandler.checkWithdrawRules((SavingsAccount) account, amountToWithdraw);
        } else {
            currentAccountHandler.checkWithdrawRules((CurrentAccount) account, amountToWithdraw);
        }

    }

    /**
     * TODO - To be implemented
     *
     * @param accountId
     * @param amountToDeposit
     * @throws AccountNotFoundException
     */
    public void deposit(Long accountId, int amountToDeposit)
            throws AccountNotFoundException {

    }
}
