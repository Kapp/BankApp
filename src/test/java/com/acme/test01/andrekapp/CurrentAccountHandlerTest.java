package com.acme.test01.andrekapp;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.fail;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class CurrentAccountHandlerTest {

    private CurrentAccountHandler currentAccountHandler;

    @Before
    public void setUp() {
        currentAccountHandler = new CurrentAccountHandler();
    }

    @After
    public void tearDown() {

    }

    @Test
    public void testWithdrawalWithinLimit() {

        Account currentAccount = SystemDB.getAccount(Long.valueOf(3));
        int withdrawlAmount = 50;
        /*
        This account is initialized with a balance of +1000
         */
        try {
            currentAccountHandler.checkWithdrawRules((CurrentAccount) currentAccount, withdrawlAmount);
        } catch (WithdrawalAmountTooLargeException we) {
            fail("Should not exceed limit");
        }
    }


    @Test
    public void testWithdrawalExceedLimit() {

        Account currentAccount = SystemDB.getAccount(Long.valueOf(3));
        int withdrawlAmount = 95000;
        /*
            Account balance is 1000, already 10000 into overdraft
         */
        try {
            currentAccountHandler.checkWithdrawRules((CurrentAccount) currentAccount, withdrawlAmount);
        } catch (WithdrawalAmountTooLargeException we) {
            assertThat(we.getMessage(), is("Withdrawal will take account below minimum balance"));
        }
    }
}
