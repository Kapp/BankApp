package com.acme.test01.andrekapp;

import org.junit.*;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

public class SystemDBTest {

    /**
     * For actual database implementation we would acquire a DB connection here. Wither in-memory or actual integration test DB.
     */
    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {

    }

    /**
     * This expects to throw and Exception
     */
    @Test(expected = AccountNotFoundException.class)
    public void testAccountDoesNotExists() {
        Long accountId = Long.valueOf(5);
        Account account = SystemDB.getAccount(accountId);
    }

    /**
     * The account should exists
     */
    @Test
    public void testGetSavingsAccount() {
        Long accountId = Long.valueOf(1);
        Account account = SystemDB.getAccount(accountId);
        assertEquals(account.getId().intValue(), 1);
    }

    /**
     * Retrieve know current account - confirm also that it is not an instance of a savings account but is actuallly a current account
     */
    @Test
    public void testGetCurrentAccount() {
        Long accountId = Long.valueOf(3);
        Account account = SystemDB.getAccount(accountId);
        assertEquals(account.getId().intValue(), 3);
        assertTrue(account instanceof CurrentAccount);
        assertFalse(account instanceof SavingsAccount);
    }

}
