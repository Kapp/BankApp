package com.acme.test01.andrekapp;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.fail;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class SavingsAccountHandlerTest {

    private SavingsAccountHandler savingsAccountHandler;

    @Before
    public void setUp() {
        savingsAccountHandler = new SavingsAccountHandler();
    }

    @After
    public void tearDown() {

    }

    @Test
    public void testWithdrawlWithinLimit() {

        Account savingsAccount = SystemDB.getAccount(Long.valueOf(1));
        int withdrawlAmount = 50;
        /*
        This account is initialized with a balance of +1000
         */
        try {
            savingsAccountHandler.checkWithdrawRules((SavingsAccount) savingsAccount, withdrawlAmount);
        } catch (WithdrawalAmountTooLargeException we) {
            fail("Should not exceed limit");
        }
    }

    @Test
    public void testWithdrawalExceedLimit() {

        Account savingsAccount = SystemDB.getAccount(Long.valueOf(1));
        int withdrawlAmount = 50000;
        /*
        This account is initialized with a balance of +1000
         */
        try {
            savingsAccountHandler.checkWithdrawRules((SavingsAccount) savingsAccount, withdrawlAmount);
        } catch (WithdrawalAmountTooLargeException we) {
            assertThat(we.getMessage(), is("Withdrawal will take account below minimum balance"));
        }
    }
}
