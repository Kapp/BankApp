package com.acme.test01.andrekapp;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class AccountServiceImplTest {

    private static AccountService accountService;

    @Before
    public void setUp() {
        accountService = new AccountServiceImpl();
    }

    @After
    public void tearDown() {

    }

    @Test
    public void testSavingsAccountWithdrawal() {

        Long accountId = Long.valueOf(1);
        int withdrawalAmount = 50;
        try {
            accountService.withdraw(accountId, withdrawalAmount);
        } catch (WithdrawalAmountTooLargeException we) {
            assertTrue(false);  // should not fail
        }
    }

    @Test
    public void testCurrentAccountWithdrawal() {

        Long accountId = Long.valueOf(3);
        int withdrawalAmount = 50;
        try {
            accountService.withdraw(accountId, withdrawalAmount);
        } catch (WithdrawalAmountTooLargeException we) {
            assertTrue(false);  // should not fail
        }
    }

    @Test
    public void testCurrentAccountWithdrawalExceed() {

        Long accountId = Long.valueOf(3);
        int withdrawalAmount = 950000;
        try {
            accountService.withdraw(accountId, withdrawalAmount);
        } catch (WithdrawalAmountTooLargeException we) {
            assertThat(we.getMessage(), is("Withdrawal will take account below minimum balance"));
        }
    }

}
